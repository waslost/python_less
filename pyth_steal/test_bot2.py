#!/usr/bin/env python

# Simple Bot to reply to Telegram messages and store info in SQLite database

"""
This Bot uses the Updater class to handle the bot.
First, the database is created (if it isn't) and a few callback functions are defined. Then,
The bot checks for past records and loads them. Finally, the callback functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
KNOWN ISSUE: '' user inputs appear as None. Unable to satisfactorily apply conditionals.
"""

#### SQLITE BRANCH ####
import sys
import re
import sqlite3


def loadDB():
    # Creates SQLite database to store info.
    conn = sqlite3.connect('content.sqlite')
    cur = conn.cursor()
    conn.text_factory = str
    cur.executescript('''CREATE TABLE IF NOT EXISTS userdata
    (
    id INTEGER NOT NULL PRIMARY KEY UNIQUE, 
    firstname TEXT, 
    Name TEXT,
    Email TEXT);''')
    conn.commit()
    conn.close()

def loadDB_SH():
    # Creates SQLite database to store info.
    conn = sqlite3.connect('content.sqlite')
    cur = conn.cursor()
    conn.text_factory = str
    cur.executescript('''CREATE TABLE IF NOT EXISTS userdata_sh
    (
    id INTEGER NOT NULL PRIMARY KEY UNIQUE, 
    firstname TEXT,
    Monday TEXT, 
    Tuesday TEXT,
    Wednesday TEXT,
    Thursday TEXT,
    Friday TEXT,
    Saturday TEXT,
    Sunday TEXT
    );''')
    conn.commit()
    conn.close()


def checkUser(update, user_data):
    # Checks if user has visited the bot before
    # If yes, load data of user
    # If no, then create a new entry in database
    conn = sqlite3.connect('content.sqlite')
    cur = conn.cursor()
    conn.text_factory = str
    if len(cur.execute('''SELECT id FROM userdata WHERE id = ?        
            ''', (update.message.from_user.id,)).fetchall()) > 0:
        c = cur.execute('''SELECT Name FROM userdata WHERE id = ?''', (update.message.from_user.id,)).fetchone()
        user_data['Name'] = c[0]
        c = cur.execute('''SELECT Email FROM userdata WHERE id = ?''', (update.message.from_user.id,)).fetchone()
        user_data['Email'] = c[0]


        print('Past user')
    else:
        cur.execute(
            '''INSERT OR IGNORE INTO userdata (id, firstname) VALUES (?, ?)''',
            (update.message.from_user.id, update.message.from_user.first_name,))
        cur.execute(
            '''INSERT OR IGNORE INTO userdata_sh (id, firstname) VALUES (?, ?)''',
            (update.message.from_user.id, update.message.from_user.first_name,))
        print('New user')
    conn.commit()
    conn.close()


def updateUser(category, text, update):
    #print(text, update.message.from_user.id)
    # Updates user info as inputted.
    print()
    print('category in UpdateUser:', category)
    print()
    print(text)

    schedule = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

    if category in schedule:
        conn = sqlite3.connect('content.sqlite')
        cur = conn.cursor()
        conn.text_factory = str
        # Update SQLite database as needed.
        cur.execute(
            '''UPDATE OR IGNORE userdata_sh SET {} = ? WHERE id = ?'''.format(category),
            (text, update.message.from_user.id,))
        conn.commit()
        conn.close()

    else:
        conn = sqlite3.connect('content.sqlite')
        cur = conn.cursor()
        conn.text_factory = str
        # Update SQLite database as needed.
        cur.execute(
            '''UPDATE OR IGNORE userdata SET {} = ? WHERE id = ?'''.format(category),
            (text, update.message.from_user.id,))
        conn.commit()
        conn.close()

#### Python Telegram Bot Branch ####

from telegram import ReplyKeyboardMarkup
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
import telegram
import telegram.ext
from telegram.ext import JobQueue
from telegram import ChatAction
import datetime
import logging


# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

logger = logging.getLogger(__name__)

CHOOSING, CHOICE, TYPING_REPLY, TYPING_CHOICE, SHOW_INFO, SCHEDULE, NOTIFICATION = range(7)

reply_keyboard = [['Name'],
                  ['Email', 'Расписание'],
                  ['Done'],
                  ['Time'],
                  ]

schedule_keyboard = [
    ['Monday'],
    ['Tuesday'],
    ['Wednesday'],
    ['Thursday'],
    ['Friday'],
    ['Saturday'],
    ['Sunday']]

settings_keyboard = [['Расписание', 'Email'],
                     ['Time']]

markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
settings_markup = ReplyKeyboardMarkup(settings_keyboard, one_time_keyboard=True)

schedule_markup = ReplyKeyboardMarkup(schedule_keyboard, one_time_keyboard=True)


def facts_to_str(user_data):
    facts = list()

    for key, value in user_data.items():
        facts.append('{} - {}'.format(key, value))

    return "\n".join(facts).join(['\n', '\n'])


def start(bot, update, user_data):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    update.message.reply_text(
        "I am Mr Meeseeks look at me!. \n"
        "Why don't you tell me something about yourself?\n"
        "Type /help to see something))\n\n"
        "Use command /skip to skip action.\n\n"
        "And now its time to change SETTINGS (add sh and smth else)",
        reply_markup=settings_markup)
    checkUser(update, user_data)
    return CHOOSING


def check_email(email):
    reg_standard = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'
    match = re.match(reg_standard, email)

    return False if match is None else True


def skip(bot, update):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    update.message.reply_text('Ok, np. Canceling')
    return CHOOSING


def set_schedule(bot, update, user_data):
    text = update.message.text
    user_data['choice'] = text

    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    update.message.reply_text(
        "KK, lets do that shit",
        reply_markup=schedule_markup)
    checkUser(update, user_data)
    global is_email
    is_email = False


def regular_choice(bot, update, user_data):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    text = update.message.text
    user_data['choice'] = text

    update.message.reply_text(
            'Your {}? Yes, I would love to hear about that!'.format(text.lower()))
    global is_email

    is_email = True if text == 'Email' else False
    return TYPING_REPLY


def select_choice(bot, update, user_data):
    text = update.message.text

    if text == 'Расписание':
        set_schedule(bot, update, user_data)
        return TYPING_CHOICE
    else:
        regular_choice(bot, update, user_data)
    return TYPING_REPLY


def received_information(bot, update, user_data):
    text = update.message.text
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)

    if is_email == True and check_email(text) == False:
        update.message.reply_text(
            f'Your Email: {text} is Invalid. \nTry again')
        return TYPING_REPLY
    else:
        category = user_data['choice']

        user_data[category] = text
        if category == 'Расписание':
            return TYPING_CHOICE

        updateUser(category, text, update)
        del user_data['choice']

        update.message.reply_text(
            "Neat! Just so you know, this is what you already told me:"
            "{}"
            "You can tell me more, or change your opinion on something.".format(facts_to_str(user_data)),
            reply_markup=markup)
    return CHOOSING


def done(bot, update, user_data):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    if 'choice' in user_data:
        del user_data['choice']

    update.message.reply_text("I learned these facts about you:"
                              "{}"
                              "Until next time!".format(facts_to_str(user_data)))

    user_data.clear()
    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def report(bot, update):
    update.message.reply_text("Kurwa check something")
    bot.send_message(chat_id=update.message.chat_id, text="I'm sorry Dave I'm afraid I can't do that.")
    print('STKA')


def callback_alarm(bot, update):

    bot.send_message(chat_id=250123658, text = "Some text!")
    print('Stka')


def check_notification(text):
    reg = '^Time-[0-9]*\:[0-9]*-(.?)*$'
    match = re.match(reg, text)

    return False if match is None else True


def callback_timer(bot, update: telegram.Update, job_queue, user_data):
    """ Running on Mon, Tue, Wed, Thu, Fri = tuple(range(5)) """
    text = update.message.text
    print(text)
    if check_notification(text) is False:
        update.message.reply_text('Try again bro invalid data')
        return NOTIFICATION
    else:
        print('ERROR')
        t = datetime.time(10, 00, 00, 000000)
        job_queue.run_daily(callback_alarm, t, days=tuple(range(5)), context=update)

        bot.send_message(
            chat_id=update.message.chat_id,
            text='Setting a daily notifications!')

        job_queue.run_once(callback=callback_alarm, when=1, context=update)
        return CHOICE


def set_notification(bot, update, job_queue):
    bot.send_message(
        chat_id=update.message.chat_id,
        text='Setting a daily notifications!\n'
             'Send me time when notificate U\n'
             'Example "Time-HOUR:MINUTE Tell my my am gay"\n'
             'Example "Time-12:50-Tell my my am gay"')
    return CHOICE


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater("")
    print("Connection to Telegram established; starting bot.")
    # Get the dispatcher to register handlers
    dp = updater.dispatcher




    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start, pass_user_data=True),
                      CommandHandler('time', set_notification, pass_job_queue=True)],


        states={
            CHOOSING: [RegexHandler(
                '^(Name|Расписание|Email|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)$',
                select_choice,
                pass_user_data=True),
                       ],

            TYPING_CHOICE: [MessageHandler(
                Filters.text,
                regular_choice,
                pass_user_data=True),
                CommandHandler('cancel', skip)
                            ],

            TYPING_REPLY: [MessageHandler(
                Filters.text,
                received_information,
                pass_user_data=True),
                CommandHandler('cancel', skip)
                           ],
            SHOW_INFO: [MessageHandler(
                Filters.text,
                received_information,
                pass_user_data=True),
                CommandHandler('cancel', skip)
                           ],
            SCHEDULE: [MessageHandler(
                Filters.all,
                report,
                pass_user_data=True),
                CommandHandler('cancel', skip)
                           ],

        },

        fallbacks=[RegexHandler('^Done$', done, pass_user_data=True)]
    )

    conv_handler_time = ConversationHandler(
        entry_points=[CommandHandler('time', set_notification, pass_job_queue=True, pass_user_data=True)],


        states={
            # NOTIFICATION: [MessageHandler(
            #     Filters.text,
            #     regular_choice,
            #     pass_user_data=True),
            #     CommandHandler('cancel', skip)
            #                 ],

            CHOICE: [RegexHandler(
                '^(^Time-(.*)$)$',
                callback_timer,
                pass_user_data=True,
                pass_job_queue=True),
                CommandHandler('cancel', skip)
            ],
        },

        fallbacks=[RegexHandler('^Done$', done, pass_user_data=True)]
    )

    dp.add_handler(conv_handler)
    #
    dp.add_handler(CommandHandler('time', set_notification, pass_job_queue=True))
    # dp.add_handler(conv_handler_time)

    dp.add_handler(RegexHandler('^Time-(.*)$', callback_timer, pass_job_queue=True, pass_user_data=True))


    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()


    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

    # schedule.every().day.at("14:56").do(report())
    # schedule.every().day.at("14:57").do(report())
    # schedule.every().day.at("14:58").do(report())
    # schedule.every().day.at("14:59").do(report())
    # schedule.run_pending()


if __name__=='__main__':

    ### load SQLite databasse ###
    loadDB()
    loadDB_SH()
    main()
