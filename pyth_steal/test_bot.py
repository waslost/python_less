# -*- coding: utf8 -*-
import telebot
import sqlite3
from telebot import types
from telebot.types import Message
import random
from uuid import uuid4
from telegram.ext import Updater, CommandHandler


import requests
import pprint

import csv


ADMIN_ID = '250123658'
CHAT_ID = 805764832
TOKEN = ''
BASE_URL = f'https://api.telegram.org/bot{TOKEN}/'
bot = telebot.TeleBot(TOKEN)

USERS = set()
# @bot.message_handler(commands=["start"])
# def start(message):
#     sent = bot.send_message(message.chat.id, "Как тебя зовут?")
#     bot.register_next_step_handler(sent, hello)
#
#
# def hello(message):
#     bot.send_message(message.chat.id, "Привет, {name}. Рад тебя видеть. "
#                                       "Пожалуйста, отправте мне свой номер для этого "
#                                       "есть  команда /phone".format(name=message.text))
#
#
# @bot.message_handler(commands=["phone"])
# def phone(message):
#     user_markup = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
#     button_phone = types.KeyboardButton(text="Отправить номер телефона", request_contact=True)
#     user_markup.add(button_phone)
#     msg = bot.send_message(message.chat.id, "Согласны ли вы предоставить ваш номер телефона для регистрации в системе?", reply_markup=user_markup)
#     bot.register_next_step_handler(msg, main_menu)


# csvFile = 'data.csv'
# with open(csvFile, newline='') as File:
#     reader = csv.reader(File)
#     for row in reader:
#         USERS.add(str(row))
#     File.close()

# @bot.message_handler(commands=['start'])
# def start(message):
#     sent = bot.send_message(message.chat.id, 'Please describe your problem.')
#     bot.register_next_step_handler(sent, hello)
#
#
# def hello(message):
#     open('problem.txt', 'w').write(str(message.chat.id) + ' | ' + message.text + '||')
#     bot.send_message(message.chat.id, 'Thank you!')
#     bot.send_message(ADMIN_ID, str(message.chat.id) + '|' + str(message.from_user.username) + ' | ' + message.text)
#


# @bot.message_handler(content_types=['text'])
# @bot.edited_message_handler(content_types=['text'])
# def command_handler(message: Message):
#     bot.send_message(message.chat.id, 'None')
#
#     user_id = message.from_user.id
#     reply = str(random.random())
#     if message.from_user.id in USERS:
#         reply += f" \n{message.from_user.username.title()}, hello again"
#     else:
#         USERS.add(user_id)
#     bot.reply_to(message, reply)


# @bot.message_handler()
# def main_menu(message: Message):
#     user_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
#     user_markup.add(*[types.KeyboardButton(name) for name in ["Расписание"]])
#     user_markup.add(*[types.KeyboardButton(name) for name in [" Доставка", " Новости"]])
#     user_markup.add(*[types.KeyboardButton(name) for name in [" Тарифы", " Контакты"]])
#     user_markup.add(*[types.KeyboardButton(name) for name in ["⚙ Настройки"]])
#
#     reply_to_message_id = message.text
#     mess = bot.send_message(message.chat.id, reply_to_message_id, reply_markup=user_markup)
#     bot.register_next_step_handler(mess, menu)
#
#
# def raspis(message: Message):
#     keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
#     keyboard.add(*[types.KeyboardButton(name) for name in ["ПН"]])
#     keyboard.add(*[types.KeyboardButton(name) for name in ["ВТ"]])
#     keyboard.add(*[types.KeyboardButton(name) for name in ["СР"]])
#     keyboard.add(*[types.KeyboardButton(name) for name in ["⏪ Назад ⏪"]])
#
#     if message.text == "⏪ Назад ⏪":
#         user_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
#         mess = bot.send_message(message.chat.id, '⏪ Назад ⏪', reply_markup=user_markup)
#         bot.send_message(message.chat.id, mess, reply_markup=keyboard)
#         bot.register_next_step_handler(mess, main_menu)
#
#
# @bot.message_handler()
# def menu(message):
#
#     if message.text == "Расписание":
#         raspis(message)
#         return
#
#     elif message.text == "⚙ Настройки":
#         j = "<b>UNI TAXI</b>\n\nпока найстройки не настроены :)"
#         keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
#         keyboard.add(*[types.KeyboardButton(name) for name in ["⏪ Назад ⏪"]])
#         bot.send_message(message.chat.id, j, parse_mode="HTML", reply_markup=keyboard)
#         return
#
#     # if message.text == " Контакты":
#     #     k = "<b>UNI TAXI</b>\n\n<b> Телефоны диспетчерской:</b>\n+99871 2001771\n+99871" \
#     #         "2007117\n\n<b> Приём водителей:</b>\n+99890 9110220\n"
#     #     keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
#     #     keyboard.add(*[types.KeyboardButton(name) for name in ["⏪ Назад ⏪"]])
#     #     bot.send_message(message.chat.id, k, parse_mode="HTML", reply_markup=keyboard)
#     #
#     # if message.text == " Тарифы":
#     #     t = "<b>Эконом: Maniz, Spark, Nexia</b>\nСтавка: 6000 сум/3км, ожидание/5мин\nДалее: 1200 сум/км, " \
#     #         "ожидание мин/400 сум\nЗагород: 1500 сум/км\n\n<b>Комфорт: Cobalt, Lacetti, Gentra</b>\nСтавка:" \
#     #         "8000 сум/3км, ожидание/5мин\nДалее: 1400 сум/км, ожидание мин/500 сум\nЗагород: 1700 сум/" \
#     #         "км\n\n<b>Комфорт: Epica, Malibu, Captiva</b>\nСтавка: 12000 сум/3км, ожидание/5мин\nДалее: 1800" \
#     #         "сум/км, ожидание мин/500 сум\nЗагород: 2200 сум/км"
#     #     keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
#     #     keyboard.add(*[types.KeyboardButton(name) for name in ["⏪ Назад ⏪"]])
#     #     bot.send_message(message.chat.id, t, parse_mode="HTML", reply_markup=keyboard)
#
#     elif message.text == "⏪ Назад ⏪":
#         user_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
#         mess = bot.send_message(message.chat.id, '⏪ Назад ⏪', reply_markup=user_markup)
#         bot.register_next_step_handler(mess, main_menu)
#
#         # markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
#         # markup.add(*[types.KeyboardButton(name) for name in [" Заказать Такси "]])
#         # markup.add(*[types.KeyboardButton(name) for name in [" Тарифы", " Контакты"]])
#         # markup.add(*[types.KeyboardButton(name) for name in ["⚙️Настройки"]])
#
# !/usr/bin/env python

# Simple Bot to reply to Telegram messages and store info in SQLite database

"""
This Bot uses the Updater class to handle the bot.
First, the database is created (if it isn't) and a few callback functions are defined. Then, 
The bot checks for past records and loads them. Finally, the callback functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
KNOWN ISSUE: '' user inputs appear as None. Unable to satisfactorily apply conditionals.
"""

#### SQLITE BRANCH ####
import sys
import sqlite3
import re


def loadDB():
    # Creates SQLite database to store info.
    conn = sqlite3.connect('content.sqlite')
    cur = conn.cursor()
    conn.text_factory = str
    cur.executescript('''CREATE TABLE IF NOT EXISTS userdata
    (
    id INTEGER NOT NULL PRIMARY KEY UNIQUE, 
    firstname TEXT, 
    Name TEXT,
    Age TEXT,
    Address TEXT,
    Amount TEXT);'''
    )
    conn.commit()
    conn.close()


def checkUser(message, user_data):
    # Checks if user has visited the bot before
    # If yes, load data of user
    # If no, then create a new entry in database
    conn = sqlite3.connect('content.sqlite')
    cur = conn.cursor()
    conn.text_factory = str
    if len(cur.execute('''SELECT id FROM userdata WHERE id = ?        
            ''', (message.from_user.id,)).fetchall()) > 0:
        c = cur.execute('''SELECT Name FROM userdata WHERE id = ?''', (message.from_user.id,)).fetchone()
        user_data['Name'] = c[0]
        c = cur.execute('''SELECT Age FROM userdata WHERE id = ?''', (message.from_user.id,)).fetchone()
        user_data['Age'] = c[0]
        c = cur.execute('''SELECT Address FROM userdata WHERE id = ?''', (message.from_user.id,)).fetchone()
        user_data['Address'] = c[0]
        c = cur.execute('''SELECT Amount FROM userdata WHERE id = ?''', (message.from_user.id,)).fetchone()
        user_data['Amount'] = c[0]
        print('Past user')
    else:
        cur.execute('''INSERT OR IGNORE INTO userdata (id, firstname) VALUES (?, ?)''', \
            (message.from_user.id, message.from_user.first_name,))
        print('New user')
    conn.commit()
    conn.close()


def updateUser(category, text, update):
    # Updates user info as inputted.
    conn = sqlite3.connect('content.sqlite')
    cur = conn.cursor()
    conn.text_factory = str
    # Update SQLite database as needed.
    cur.execute('''UPDATE OR IGNORE userdata SET {} = ? WHERE id = ?'''.format(category), \
        (text, update.message.from_user.id,))
    conn.commit()
    conn.close()


#### Python Telegram Bot Branch ####

from telegram import ReplyKeyboardMarkup
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
import logging

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

logger = logging.getLogger(__name__)

CHOOSING, TYPING_REPLY, TYPING_CHOICE = range(3)

reply_keyboard = [['Name', 'Age'],
                  ['Address', 'Amount'],
                  ['Done']]


markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)



def facts_to_str(user_data):
    facts = list()

    for key, value in user_data.items():
        facts.append('{} - {}'.format(key, value))

    return "\n".join(facts).join(['\n', '\n'])


def get_name(username):
    pattern = "^\('(.*?)',\)$"
    test_string = username
    if len(username) > 0:
        result = re.search(pattern, test_string)
        result = result.group(1)
    else:
        result = ''
    return result


@bot.message_handler(commands=['start'])
def start(message: Message):
    conn = sqlite3.connect('content.sqlite')
    cur = conn.cursor()
    conn.text_factory = str
    bot.reply_to(message, 'None')
    user_data = {}

    if len(cur.execute('''
                SELECT id 
                FROM userdata 
                WHERE id = ?''', (message.from_user.id,)).fetchall()) > 0:

        username = cur.execute('''
            SELECT firstname 
            FROM userdata 
            WHERE id = ?''', (message.from_user.id,)).fetchone()

        username = get_name(str(username))
        mess = f"Hi, {username}! I am Mr Meeseeks look at me!. "

        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.add(*[types.KeyboardButton(name) for name in ["Name", "Age", "Address", "Amount"]])
        bot.send_message(message.chat.id, mess, reply_markup=keyboard)

    else:
        mess = " I am Mr Meeseeks look at me!.\n Tell me about urself"
        bot.send_message(message.chat.id, mess)
        checkUser(message, user_data)

    return CHOOSING


def regular_choice(bot, update, user_data):
    text = update.message.text
    user_data['choice'] = text
    update.message.reply_text(
        'Your {}? Yes, I would love to hear about that!'.format(text.lower()))
    return TYPING_REPLY


def received_information(bot, update, user_data):
    text = update.message.text
    category = user_data['choice']
    user_data[category] = text
    updateUser(category, text, update)
    del user_data['choice']

    update.message.reply_text("Neat! Just so you know, this is what you already told me:"
                              "{}"
                              "You can tell me more, or change your opinion on something.".format(
        facts_to_str(user_data)), reply_markup=markup)
    return CHOOSING


def done(bot, update, user_data):
    if 'choice' in user_data:
        del user_data['choice']

    update.message.reply_text("I learned these facts about you:"
                              "{}"
                              "Until next time!".format(facts_to_str(user_data)))

    user_data.clear()
    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater(TOKEN)
    print("Connection to Telegram established; starting bot.")
    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start, pass_user_data=True)],

        states={
            CHOOSING: [RegexHandler('^(Name|Age|Address|Amount)$',
                regular_choice,
                pass_user_data=True),
                       ],

            TYPING_CHOICE: [MessageHandler(Filters.text,
                regular_choice,
                pass_user_data=True),
                            ],

            TYPING_REPLY: [MessageHandler(Filters.text,
                received_information,
                pass_user_data=True),
                           ],
        },

        fallbacks=[RegexHandler('^Done$', done, pass_user_data=True)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)
    bot.polling(none_stop=True, timeout=123)
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    loadDB()
    main()


