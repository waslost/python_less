from tkinter import *
import random
import time


class Ball:
    def __init__(self, canvas, paddle, color):
        self.canvas = canvas
        self.paddle = paddle
        self.id = canvas.create_oval(10, 10, 30, 30, fill=color)
        self.canvas.move(self.id, 245, 100)
        starts = [-3, -2, -1, 1, 2, 3]
        random.shuffle(starts)
        self.x = starts[0]
        self.y = starts[1]
        self.canvas_height = self.canvas.winfo_height()
        self.canvas_width = self.canvas.winfo_width()
        self.hit_bottom = False

    def draw(self):
        self.canvas.move(self.id, self.x, self.y)
        pos = self.canvas.coords(self.id)

        if pos[3] >= self.canvas_height:
            self.hit_bottom = True

        if pos[1] <= 0:
            self.y = 3
        if pos[3] >= self.canvas_height:
            self.y = -3

        if pos[2] >= self.canvas_width:
            self.x = -3
        if pos[0] <= 0:
            self.x = 3
            # if pos[2] >= self.canvas_width:
               # self.x = 3

        if self.hit_paddle(pos):
            self.y = -3

    def hit_paddle(self, pos):

        paddle_pos = self.canvas.coords(self.paddle.id)
        print(paddle_pos)
        if pos[2] >= paddle_pos[0] and pos[0] <= paddle_pos[2]:
            if pos[3] >= paddle_pos[1] and pos[3] <= paddle_pos[3]:
                return True

        return False


class Paddle:

    def __init__(self, canvas, color):
        self.canvas = canvas
        self.id = canvas.create_rectangle(0, 0, 100, 10, fill=color)
        self.canvas.move(self.id, 200, 400)
        self.x = 0
        self.canvas_width = self.canvas.winfo_width()
        self.canvas.bind_all('<KeyPress-Left>', self.turn_left)
        self.canvas.bind_all('<KeyPress-Right>', self.turn_right)

    def draw(self):
        self.canvas.move(self.id, self.x, 0)
        pos = self.canvas.coords(self.id)

        if pos[0] <= 0:
            self.x = 0
        elif pos[2] >= self.canvas_width:
            self.x = 0

    def turn_left(self, e):
        self.x = -2

    def turn_right(self, e):
        self.x = 2


if __name__ == '__main__':

    root = Tk()
    root.title('Game')
    root.resizable(0, 0)
    root.wm_attributes('-topmost', 1)
    start = False

    canvas = Canvas(root, width=500, height=500, bd=0, highlightthickness=0)
    canvas.pack()

    root.update()
    paddle = Paddle(canvas, 'blue')
    ball = Ball(canvas, paddle, 'red')

    while 1:
        if not ball.hit_bottom:
            ball.draw()
            paddle.draw()
        root.update()
        time.sleep(0.015)







