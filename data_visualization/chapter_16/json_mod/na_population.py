from pygal.maps.world import World

wm = World()
wm.title = 'Population of Countries in North America'
wm.add('North America', {'ca': 34126000, 'us': 30934900, 'mx': 11342300})
wm.render_to_file('na_population.svg')
