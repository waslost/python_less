import random

NUM_DIGITS = 3
MAX_GUESS = 10


def get_secret_num():
    # Returns a string of unique random digits that is NUM_DIGITS long.
    numbers = random.randint(10)
    random.shuffle(numbers)

    secret_num = ''
    for i in range(NUM_DIGITS):
        secret_num += str(numbers[i])
    return secret_num


if __name__ == '__main__':
    pass


