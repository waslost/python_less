from django.contrib import admin
from django.conf.urls import url, include
from learning_logs import views
from django.urls import path
from django.contrib.auth.views import LoginView

urlpatterns = [
    # Homepage
    path('', views.index, name='index'),
    # Вывод всех тем
    url(r'^topics/$', views.topics, name='topics'),
    url(r'^topics/(?P<topic_id>\d+)/$', views.topic, name='topic'),
    # Страница для добавления новой темы
    url(r'^new_topic/$', views.new_topic, name='new_topic'),
    url(r'^new_entry/(?P<topic_id>\d+)/$', views.new_entry, name='new_entry'),
    # Страница для редактирования записи
    url(r'^edit_entry/(?P<entry_id>\d+)/$', views.edit_entry, name='edit_entry'),
    # Страница входа
    url(r'^login/$', LoginView.as_view(template_name='users/login.html'), name='login'),


]
